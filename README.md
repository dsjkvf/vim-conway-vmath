# Doing math on visual regions

## About

This is a Vim plugin for performing basic math operations on visual regions. Originally written by [Damian Conway](http://damian.conway.org/About_us/Bio_formal.html), copied now from [Damian's collection](https://github.com/thoughtstream/Damian-Conway-s-Vim-Setup) of his Vim-related files to a separate repository in order to make the installation process easier.

## Configuration

Add the following to your `.vimrc`:

    vnoremap <silent><expr> ++ VMATH_YankAndAnalyse()
    nnoremap <silent> ++ vip++

## Appendix

As an alternative, one may be also interested in [visSum.vim](https://github.com/vim-scripts/visSum.vim) plugin.
